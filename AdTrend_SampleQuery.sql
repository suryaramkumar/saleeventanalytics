(
  SELECT AD_METRICS_AGG.*,
  CASE WHEN  IMPRESSIONS_PREV = 0 THEN NULL ELSE IMPRESSIONS / IMPRESSIONS_PREV END AS IMPRESSIONS_PERCT_INC,
  CASE WHEN  SPEND_PREV = 0 THEN NULL ELSE SPEND / SPEND_PREV END AS SPEND_PERCT_INC,
  
  AD_METRICS_AGG.REPORT_DATE BETWEEN '2019-07-15' AND '2019-07-16' AS PRIME_DAY_2019,
  AD_METRICS_AGG.REPORT_DATE BETWEEN '2020-10-13' AND '2020-10-14' AS PRIME_DAY_2020,
  AD_METRICS_AGG.REPORT_DATE BETWEEN DATEADD(WEEK,-2,'2019-07-15') AND DATEADD(WEEK,2,'2019-07-16') AS PRIME_SEASON_2019,
  AD_METRICS_AGG.REPORT_DATE BETWEEN DATEADD(WEEK,-2,'2020-10-13') AND DATEADD(WEEK,2,'2020-10-14') AS PRIME_SEASON_2020
-- get ad metrics for top categories

FROM ( -- get top subcategories FOR ALL DATES
      SELECT A.*, B.*
      FROM (
        SELECT CATEGORY, SUBCATEGORY
        FROM (SELECT * FROM brands_centralpet.brands_cubes.sales_dashboard_widget_data WHERE CLIENTDETAILSID = 549)
        WHERE SUBCATEGORY IN 
            (
              SELECT subcategory
              from (
                select category, subcategory, ratio_to_report (sales) over () as total_sales
                from (
                  select category, subcategory, 
                  sum(ordered_revenue) as sales
                  from brands_centralpet.brands_cubes.sales_dashboard_widget_data
                  where feed_date between '2020-01-01' and CURRENT_DATE()
                  and subcategory is not null
                  AND CATEGORY IS NOT NULL
                  AND CLIENTDETAILSID = 549
                  group by 1,2
                  ORDER BY 3 DESC NULLS LAST
                )
              )
            where total_sales >= 0.01
            )
            GROUP BY 1,2
        ) A

    -- GET LIST OF ALL DATES
        CROSS JOIN
        (
        select
          dateadd
          (
            day,
            '-' || row_number() over (order by null),
            dateadd(day, '+1', CURRENT_DATE())
          ) as REPORT_DATE
          from table (generator(rowcount => 639))
        ) B
    ) TOP_CATEGORIES
    
LEFT JOIN 

    ( -- get metrics at category*feed_date level
    SELECT CLIENT_ID, CATEGORY, SUBCATEGORY, REPORT_DATE, 
      ANY_VALUE(REPORT_DATE_YYW) AS YYW, ANY_VALUE(REPORT_DATE_YY) AS YY,
      SUM(DAILY_BUDGET) AS DAILY_BUDGET, SUM(SPEND) AS SPEND, SUM(IMPRESSIONS) AS IMPRESSIONS, SUM(CLICKS) AS CLICKS, 
      SUM(HALO_SALES_1_DAY) AS HALO_SALES_1_DAY, SUM(ORDERS_1D) AS ORDERS_1D, SUM(UNITS_1D) AS UNITS_1D, 
      AVG(CTR) AS CTR, AVG(CONVERSIONS_1D) AS CONVERSIONS_1D, AVG(ACOS_1D) AS ACOS_1D, AVG(ROI) AS ROI, 
      SUM(PAIDSALES_14D) AS PAIDSALES_14D,

      SUM(IMPRESSIONS_TA4) AS IMPRESSSIONS_TA4, SUM(SPEND_TA4) AS SPEND_TA4,
      AVG(CONVERSIONS_1D_TA4) AS CONVERSIONS_1D_TA4, AVG(CTR_TA4) AS CTR_TA4,
      
      SUM(IMPRESSIONS_PREV) AS IMPRESSIONS_PREV,
      SUM(SPEND_PREV) AS SPEND_PREV
      
      

    FROM ( -- get metrics at campaign*asin*feed_date level
        select AD_METRICS.*,
      
          AVG(IMPRESSIONS) over (PARTITION BY AD_METRICS.CAMPAIGN_ID, AD_METRICS.ASIN ORDER BY REPORT_DATE ASC rows 4 preceding) as IMPRESSIONS_TA4,
          AVG(CTR) OVER (PARTITION BY AD_METRICS.CAMPAIGN_ID, AD_METRICS.ASIN ORDER BY REPORT_DATE ASC rows 4 preceding) as CTR_TA4,
          AVG(CONVERSIONS_1D) over (PARTITION BY AD_METRICS.CAMPAIGN_ID, AD_METRICS.ASIN ORDER BY REPORT_DATE ASC rows 4 preceding) as CONVERSIONS_1D_TA4,
          AVG(SPEND) over (PARTITION BY AD_METRICS.CAMPAIGN_ID, AD_METRICS.ASIN ORDER BY REPORT_DATE ASC rows 4 preceding) as SPEND_TA4,
      
          LAG(IMPRESSIONS,1) OVER (PARTITION BY AD_METRICS.CAMPAIGN_ID, AD_METRICS.ASIN ORDER BY REPORT_DATE) as IMPRESSIONS_PREV,
          LAG(SPEND,1) OVER (PARTITION BY AD_METRICS.CAMPAIGN_ID, AD_METRICS.ASIN ORDER BY REPORT_DATE) as SPEND_PREV
      

        from 
            (
              SELECT DISTINCT CLIENT_ID, CATEGORY, SUBCATEGORY, CAMPAIGN_ID, ASIN,
                REPORT_DATE,

                CONCAT(DATE_PART(YEAR, report_date),'-', LPAD(DATE_PART(WEEK, report_date),2,'0')) AS REPORT_DATE_YYW,
                DATE_PART(YEAR, report_date) AS REPORT_DATE_YY,

                ANY_VALUE(DAILY_BUDGET) AS DAILY_BUDGET, 
                sum(COALESCE(COST,0)) AS SPEND, 
                sum(COALESCE(IMPRESSIONS,0)) AS IMPRESSIONS,
                case when sum(IMPRESSIONS)= 0 THEN NULL ELSE sum(CLICKS) / sum(IMPRESSIONS) END * 100 AS CTR, 
                CASE WHEN SUM(COALESCE(IMPRESSIONS,0)) = 0 THEN NULL ELSE sum(CLICKS) END AS CLICKS, 
                case when sum(clicks)= 0 THEN NULL ELSE sum(cost) / sum(clicks) END AS CPC, 
                case when sum(clicks)= 0 THEN NULL ELSE sum(attributedconversions1d) / sum(clicks) END * 100 AS conversions_1d, 
                case when sum(attributedsales1d)= 0 THEN NULL ELSE sum(cost) / sum(attributedsales1d) END * 100 AS acos_1d,
                sum(attributedsales1d) - sum(attributedSales1dSameSKU) AS halo_sales_1_day,
                CASE WHEN any_value(bidplus) = 'true' THEN 'Yes' ELSE 'No' END AS bidplus, 
                round(
                  case when (
                    case when sum(attributedsales14d)= 0 THEN NULL ELSE sum(cost) / sum(attributedsales14d) END
                  ) = 0 THEN 0 ELSE (
                    1 / (
                      case when sum(attributedsales14d)= 0 THEN NULL ELSE sum(cost) / sum(attributedsales14d) END
                    )
                  ) END, 
                  2
                ) AS roi, 
                sum(attributedsales14d) AS paidsales_14d, 
                sum(attributedconversions1d) AS orders_1d, 
                sum(attributedunitsordered1d) AS units_1d, 
                any_value(campaign_type) AS campaign_type, 
                any_value(serving_status) AS serving_status

              from (
                SELECT  DISTINCT end_date, DAILY_BUDGET,
                  adgroup_id, attributedsales30d, attributedconversions1d, campaign_type, attributedSales1dSameSKU, campaign_state, client_id, bidplus, 
                  state_latest, portfolio_id, ad_serving_status, campaign_state_daily, ordered_revenue, campaign_id, report_date, PROFILE_ID, cost, impressions, 
                  attributedsales1d, attributedUNITSORDERED1d, attributedsales14d, serving_status, ad_id, clicks, asin, category, subcategory, active_campaign 
                FROM brands_centralpet.ams_cubes.campaigns_asin_workbench 
                WHERE CLIENT_ID = '549') X
              WHERE report_date between '2019-03-01' and CURRENT_DATE()
              group by 1,2,3,4,5,6
            ) AD_METRICS
        
        )

    GROUP BY 1,2,3,4
    ORDER BY 1
    ) AD_METRICS_AGG
    
ON TOP_CATEGORIES.CATEGORY = AD_METRICS_AGG.CATEGORY
AND TOP_CATEGORIES.SUBCATEGORY = AD_METRICS_AGG.SUBCATEGORY
AND TOP_CATEGORIES.REPORT_DATE = AD_METRICS_AGG.REPORT_DATE

ORDER BY 1,2,3
);