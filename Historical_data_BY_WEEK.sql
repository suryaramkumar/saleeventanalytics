
with asin_mappings as (
-- pull relevant asin mappings
    select distinct asin, dimension8 as brand, dimension9 as category 
    from aramus.client_internal_catalog
    where clientdetailsid = '877'
    qualify row_number() over (partition by asin order by creation_date desc) = 1

)

, base_data as (
— get data for aggregation
    select distinct  feed_date, asin, ordered_revenue, ordered_units from aramus.sales_diagnostic_details
    where clientdetailsid =  '877'
)

, agg_data as (
-- SF aggregates week as Mon - Sun, but we need Sun - Sat. Hence using “feed_date + 1”
    select 
        b.category, b.brand,
        concat(date_part('yearofweek',feed_date+1),'-',lpad(date_part('weekofyear',feed_date+1), 2,'0')) as "Year-Week#", 
  
        sum(ordered_revenue) as "OPS",
        sum(ordered_units) as "Units"
  
    from base_data a
  
    left join asin_mappings b
    on a.asin = b.asin
  
    group by 1,2,3

)

select * from agg_data
order by category, brand, "Year-Week#";


/* 

--check week numbers

--date generator
with list_of_dates as (
select
  dateadd
  (
    day,
    '-' || row_number() over (order by null),
    dateadd(day, '+1', CURRENT_DATE())
  ) as feed_date
from table (generator(rowcount => 4000))
)

select *, concat(date_part('yearofweek',feed_date+1),'-',lpad(date_part('weekofyear',feed_date+1), 2,'0')) as "Year-Week#"
from list_of_dates
order by 1 desc
;

*/

--select max(feed_date) from  aramus.sales_diagnostic_details;
