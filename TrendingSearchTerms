SET PRIMEDATE = '2020-10-13';
SET PRIMEDAYDURATION = 1;

DROP TABLE IF EXISTS BRANDS.TEMP.PMD_SEARCHTERM_RANKS;

CREATE TABLE BRANDS.TEMP.PMD_SEARCHTERM_RANKS AS 
with search_term_data as (
  select distinct  feed_date, department, search_term, search_frequency_rank,  
    CASE WHEN FEED_DATE BETWEEN $PRIMEDATE AND DATEADD(DAY, $PRIMEDAYDURATION, $PRIMEDATE) THEN TRUE ELSE FALSE END AS PRIMEDAY_FLAG
  from brands.aramus.search_keyword_report
  where feed_date  between DATEADD(DAY, -14, $PRIMEDATE) and DATEADD(DAY, $PRIMEDAYDURATION, $PRIMEDATE)
  and RETAILER_ATTRIBUTES_ID = 1
  and search_frequency_rank <= 1000
  order by feed_date, department, search_frequency_rank asc
) ,
 search_term_list as (
  select distinct department, search_term
  from search_term_data
),
search_terms_grouped as (
  select distinct a.department, a.search_term, b.feed_date, c.PRIMEDAY_FLAG, b.search_frequency_rank, c.mean_rank, c.best_rank, c.worst_rank, c.num_days,
     search_frequency_rank - lag(search_frequency_rank, 1) over (partition by b.department, b.search_term order by feed_date asc) as rank_daily_change
  from search_term_list a
  left join search_term_data b
  on a.search_term = b.search_term
  and a.department = b.department
  left join (
        select department, search_term, primeday_flag,
          min (search_frequency_rank) as best_rank,
          max (search_frequency_rank) as worst_rank,
          avg (search_frequency_rank) as mean_rank, 
          count(feed_date) as num_days 
        from search_term_data group by 1, 2, 3
  ) c
  on b.search_term = c.search_term
  and b.department = c.department
  and b.primeday_flag = c.primeday_flag
), 
search_terms_grouped2 as (
 select a.*,
   first_value(search_frequency_rank) over (partition by department, search_term order by feed_date asc) start_rank,  
   last_value (search_frequency_rank) over (partition by department, search_term order by feed_date asc) latest_rank,
   latest_rank - start_rank as rank_net_change,
   case when rank_daily_change >=0 then rank_daily_change else null end as rank_increment,
   case when rank_daily_change <0 then rank_daily_change else null end as rank_decrement
 from search_terms_grouped a
)

SELECT A.DEPARTMENT, A.SEARCH_TERM, 
    ANY_VALUE(A.START_RANK) AS START_RANK, ANY_VALUE(A.LATEST_RANK) AS LATEST_RANK, 
    ANY_VALUE(A.RANK_INCREMENT) AS NET_RANK_INCREMENT, ANY_VALUE(A.RANK_DECREMENT) AS NET_RANK_DECREMENT,
    AVG(B.MEAN_RANK) AS PMD_MEAN_RANK, AVG(B.BEST_RANK) AS PMD_BEST_RANK, 
    AVG(C.MEAN_RANK) AS NPMD_MEAN_RANK, AVG(C.BEST_RANK) AS NPMD_BEST_RANK,
    PMD_MEAN_RANK / NPMD_MEAN_RANK AS RANK_PERCT_CHANGE
FROM search_terms_grouped2 A
LEFT JOIN (
    SELECT DEPARTMENT, SEARCH_TERM, AVG(MEAN_RANK) AS MEAN_RANK, AVG (BEST_RANK) AS BEST_RANK 
    FROM search_terms_grouped2 WHERE PRIMEDAY_FLAG = TRUE
    GROUP BY 1,2
    ) B
ON A.DEPARTMENT = B.DEPARTMENT
AND A.SEARCH_TERM = B.SEARCH_TERM
LEFT JOIN (
    SELECT DEPARTMENT, SEARCH_TERM, AVG(MEAN_RANK) AS MEAN_RANK, AVG (BEST_RANK) AS BEST_RANK 
    FROM search_terms_grouped2 WHERE PRIMEDAY_FLAG = FALSE
    GROUP BY 1,2
    ) C
ON A.DEPARTMENT = C.DEPARTMENT
AND A.SEARCH_TERM = C.SEARCH_TERM
GROUP BY 1,2;

SELECT * FROM BRANDS.TEMP.PMD_SEARCHTERM_RANKS
ORDER BY DEPARTMENT ASC, RANK_PERCT_CHANGE ASC NULLS LAST;
  
--GET SEARCH TERMS WITH BIGGEST SPIKE FROM EACH DEPARTMENT
SELECT * FROM BRANDS.TEMP.PMD_SEARCHTERM_RANKS
QUALIFY ROW_NUMBER() OVER (PARTITION BY DEPARTMENT ORDER BY RANK_PERCT_CHANGE ASC) <=30;
--ORDER BY DEPARTMENT ASC, RANK_PERCT_CHANGE ASC NULLS LAST;

--GET SEARCH TERMS WITH BIGGEST SPIKE FROM ACROSS DEPARTMENTS
SELECT * FROM BRANDS.TEMP.PMD_SEARCHTERM_RANKS
ORDER BY RANK_PERCT_CHANGE ASC
LIMIT 30;

--TOP SEARCH TERMS BY DEPARTMENT: PMD VS NOT PRIME DAY
SELECT *, ROW_NUMBER() OVER (PARTITION BY EVENT_TYPE, DEPARTMENT ORDER BY MEAN_RANK ASC) AS SEARCH_RANK 
FROM (
  ( SELECT 'PRIMEDAY' AS EVENT_TYPE, DEPARTMENT, SEARCH_TERM, PMD_MEAN_RANK AS MEAN_RANK FROM BRANDS.TEMP.PMD_SEARCHTERM_RANKS
  QUALIFY ROW_NUMBER() OVER (PARTITION BY DEPARTMENT ORDER BY PMD_MEAN_RANK ASC) <=20 )
  UNION 
  ( SELECT 'NOT PRIMEDAY' AS EVENT_TYPE, DEPARTMENT, SEARCH_TERM, NPMD_MEAN_RANK AS MEAN_RANK FROM BRANDS.TEMP.PMD_SEARCHTERM_RANKS
  QUALIFY ROW_NUMBER() OVER (PARTITION BY DEPARTMENT ORDER BY NPMD_MEAN_RANK ASC) <=20 )
)
ORDER BY DEPARTMENT ASC, EVENT_TYPE, SEARCH_RANK ASC NULLS LAST;


--TOP SEARCH TERMS OVERALL: PMD VS NOT PRIME DAY
SELECT *, ROW_NUMBER() OVER (PARTITION BY EVENT_TYPE ORDER BY MEAN_RANK ASC) AS SEARCH_RANK 
FROM (
  ( SELECT 'PRIMEDAY' AS EVENT_TYPE, SEARCH_TERM, PMD_MEAN_RANK AS MEAN_RANK FROM BRANDS.TEMP.PMD_SEARCHTERM_RANKS
  QUALIFY ROW_NUMBER() OVER (ORDER BY PMD_MEAN_RANK ASC) <=20 )
  UNION 
  ( SELECT 'NOT PRIMEDAY' AS EVENT_TYPE, SEARCH_TERM, NPMD_MEAN_RANK AS MEAN_RANK FROM BRANDS.TEMP.PMD_SEARCHTERM_RANKS
  QUALIFY ROW_NUMBER() OVER (ORDER BY NPMD_MEAN_RANK ASC) <=20 )
)
ORDER BY EVENT_TYPE, SEARCH_RANK ASC NULLS LAST;
